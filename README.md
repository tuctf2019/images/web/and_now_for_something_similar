# And Now, For Something Similar -- Web -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/web/and_now_for_something_similar)

## Chal Info

Desc: `We made another attempt to login securely. Is this one any better than the last?`

Flag: `TUCTF{m4k3_y0ur53lf_4_u53r:_th3_l4zy_50lut10n?}`

## Dependencies

This relies on a MySQL database. Currently the creds are hardcoded, so there needs to be a database called `challenge` with the root password as `funnewpasswordtimes`. In the database, there should be a `users` table:

```sql
CREATE TABLE users (user VARCHAR(20) NOT NULL, password VARCHAR(50) NOT NULL);
```

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/and_now_for_something_similar)

Ports: 80

Environment Variables:

* MYSQL_ADDRESS: The address of the MYSQL server

Example usage:

```bash
docker run -d -p 127.0.0.1:8080:80 -e MYSQL_ADDRESS="127.0.0.1" asciioverflow/and_now_for_something_similar:tuctf2019
```
